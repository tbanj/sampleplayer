package com.example.engrtemitope.sampleplayer;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Toast mToastPlay;
    private Toast mToastPause;
    private MediaPlayer mediaPlayer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*Mediaplayer.create()
        * is an example of static class cos we didnt declare the Mediaplayer variable
         * by ourselves is already in the android studio
        * */
        mediaPlayer = MediaPlayer.create(this, R.raw.color_black);
        Button audioPlay = (Button) findViewById(R.id.audioplay_key);
        audioPlay.setOnClickListener(new View.OnClickListener() {
            // The code in this method will be executed when the numbers category is clicked on.
            @Override
            public void onClick(View view) {
                // Create a new intent to open the {@link NumbersActivity}
                //mToastPlay.makeText(view.getContext(), "The play button is pressed",Toast.LENGTH_SHORT).show();

                /*mediaPlayer.start()
                * is an example of non-static class cos we declare the mediaPlayer variable
                 * by ourselves is already in the android studio
                * */
                mediaPlayer.start();
                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        Toast.makeText(MainActivity.this, "Media Completed with Success", Toast.LENGTH_SHORT).show();
                    }
                });



            }
        });


        Button audioPause = (Button) findViewById(R.id.audiopause_key);
        audioPause.setOnClickListener(new View.OnClickListener() {
            // The code in this method will be executed when the numbers category is clicked on.
            @Override
            public void onClick(View view) {
                // Create a new intent to open the {@link NumbersActivity}
                //mToastPause.makeText(view.getContext(), "The pause button is pressed",Toast.LENGTH_SHORT).show();
                mediaPlayer.pause();
            }
        });


        Button audioStop = (Button) findViewById(R.id.audiostop_key);
        audioPause.setOnClickListener(new View.OnClickListener() {
            // The code in this method will be executed when the numbers category is clicked on.
            @Override
            public void onClick(View view) {
                // Create a new intent to open the {@link NumbersActivity}
                //mToastPause.makeText(view.getContext(), "The pause button is pressed",Toast.LENGTH_SHORT).show();
                mediaPlayer.reset();
            }
        });




    }
    @Override
    protected void onStart() {
        super.onStart();
        Log.v("MainActivity", "onStart"+ "This method is activated when the app is launch + \n" +
                " by the user which run simultanously with onCreate() and onResume() method  ");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.v("MainActivity", "onResume " +"onResume() is call when the user go back to view the \n" +
                "minimize which he was interacting with before");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.v("MainActivity", "onPause "+ "it runs together with onStop() but its the first method \n" +
                "to be called before onStop()");
    }
    @Override
    protected void onStop() {
        super.onStop();
        Log.v("MainActivity", "onStop"+ "This method is activated when the app is minimize + \n" +
                " by the user ");
        releaseMediaPlayer();

    }
    private void releaseMediaPlayer() {
        // If the media player is not null, then it may be currently playing a sound.
        if (mediaPlayer != null) {
            // Regardless of the current state of the media player, release its resources
            // because we no longer need it.
            mediaPlayer.release();

            // Set the media player back to null. For our code, we've decided that
            // setting the media player to null is an easy way to tell that the media player
            // is not configured to play an audio file at the moment.
            mediaPlayer = null;
        }
    }

}
